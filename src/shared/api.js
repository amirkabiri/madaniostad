import axios from "axios";
import { API_PROXY_ENABLE, API_URL, API_PROXY_URL } from "./config";
import { dangerToast } from "./toast";
import { getToken } from "./token";

const api = axios.create({
  baseURL: (API_PROXY_ENABLE ? API_PROXY_URL : "") + API_URL,
  timeout: 7000,
  headers: {
    "X-Requested-With": "http://localhost:3000",
  },
});
api.interceptors.request.use(
  function (config) {
    const token = getToken();
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (e) {
    if (e.isAxiosError) {
      if (e.response.status >= 500) {
        dangerToast("خطای سرور", "خطای غیرمنتظره ای رخ داده است");
      } else if (
        e.response.status < 500 &&
        e.response.status >= 400 &&
        Array.isArray(e.response.data.errors)
      ) {
        dangerToast("خطا!", e.response.data.errors[0]);
      }
    }
    return Promise.reject(e);
  }
);
export default api;

export function apiSuccessOutput({ data }) {
  return data.data;
}

export async function apiHandleError(asyncFunction, defaultValue) {
  try {
    return await asyncFunction();
  } catch (e) {
    console.log("apiHandleError", e);
    return defaultValue;
  }
}
