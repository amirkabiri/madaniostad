const TOKEN_KEY = "token";

export function getToken() {
  try {
    const token = localStorage.getItem(TOKEN_KEY);

    if (typeof token !== "string" || !token.length) return null;

    return token;
  } catch (e) {
    return null;
  }
}

export function setToken(token) {
  try {
    localStorage.setItem(TOKEN_KEY, token);
    return true;
  } catch (e) {
    return false;
  }
}

export function deleteToken() {
  try {
    delete localStorage.removeItem(TOKEN_KEY);
    return true;
  } catch (e) {
    return false;
  }
}
