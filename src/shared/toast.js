export function dangerToast(title, message) {
  return toast(title, message, "red");
}

export function successToast(title, message) {
  return toast(title, message, "green");
}

export function toast(title, message, color) {
  return (
    isToastAvailable() &&
    window.iziToast.show({
      title,
      message,
      color,
    })
  );
}

export const isToastAvailable = () =>
  typeof window !== "undefined" && !!window.iziToast;
