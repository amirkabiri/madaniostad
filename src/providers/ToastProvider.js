import React, { useEffect } from "react";
import { isToastAvailable } from "../shared/toast";

export default function ToastProvider({ children }) {
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (!isToastAvailable()) return;

      window.iziToast.settings({
        timeout: 6000,
        resetOnHover: true,
        icon: "material-icons",
        rtl: true,
      });
    }, 500);

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return children;
}
