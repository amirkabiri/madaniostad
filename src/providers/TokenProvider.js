import React, { useEffect } from "react";
import { useAuth } from "contexts/Auth";
import { getToken } from "shared/token";

export default function TokenProvider({ children }) {
  const { login } = useAuth();

  useEffect(() => {
    const token = getToken();

    if (token) {
      login(token);
    }
  }, []);

  return children;
}
