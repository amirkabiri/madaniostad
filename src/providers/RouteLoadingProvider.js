import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

export default function RouteLoadingProvider({ children }) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const routeChangeStart = () => setLoading(true);
  const routeChangeComplete = () => setLoading(false);

  useEffect(() => {
    router.events.on("routeChangeStart", routeChangeStart);
    router.events.on("routeChangeComplete", routeChangeComplete);

    return () => {
      router.events.off("routeChangeStart", routeChangeStart);
      router.events.on("routeChangeComplete", routeChangeComplete);
    };
  }, []);

  if (!loading) return children;
  return (
    <>
      <div
        className="fixed w-full h-full left-0 right-0 top-0 bottom-0 flex flex-col justify-center items-center z-50"
        style={{ background: "rgba(255,255,255,.8)" }}
      >
        <h1 className="text-gray-700 text-3xl">لطفا صبر کنید</h1>
        <p className="text-gray-600 text-lg mt-3">
          در حال انتقال به صفحه ای دیگر
        </p>
      </div>
      {children}
    </>
  );
}
