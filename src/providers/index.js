import React from "react";

import { AuthProvider as AuthContextProvider } from "contexts/Auth";
import { ModalProvider as ModalContextProvider } from "contexts/Modal";

import ToastProvider from "providers/ToastProvider";
import ModalProvider from "providers/ModalProvider";
import TokenProvider from "providers/TokenProvider";
import CloseModalOnRouteChangeProvider from "./CloseModalOnRouteChangeProvider";
import RouteLoadingProvider from "./RouteLoadingProvider";

export default function Providers({ children }) {
  return (
    <ToastProvider>
      <AuthContextProvider>
        <ModalContextProvider>
          <ModalProvider>
            <TokenProvider>
              <CloseModalOnRouteChangeProvider>
                <RouteLoadingProvider>{children}</RouteLoadingProvider>
              </CloseModalOnRouteChangeProvider>
            </TokenProvider>
          </ModalProvider>
        </ModalContextProvider>
      </AuthContextProvider>
    </ToastProvider>
  );
}
