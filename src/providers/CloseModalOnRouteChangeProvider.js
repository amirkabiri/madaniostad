import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useModal } from "contexts/Modal";

export default function CloseModalOnRouteChangeProvider({ children }) {
  const router = useRouter();
  const { close: closeModal } = useModal();

  const handleRouteChange = () => closeModal();

  useEffect(() => {
    router.events.on("routeChangeStart", handleRouteChange);
    return () => router.events.off("routeChangeStart", handleRouteChange);
  }, []);

  return children;
}
