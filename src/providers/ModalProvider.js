import React, { useEffect, useContext } from "react";
import { ModalContext } from "../contexts/Modal";
import cn from "classnames";

export default function ModalProvider({ children }) {
  const { isOpen, close, body } = useContext(ModalContext);

  const onContainerClick = ({ target }) => {
    target.classList.contains("close-modal") && close();
  };

  useEffect(() => {
    isOpen && (document.body.style.overflow = "hidden");
    !isOpen && (document.body.style.overflow = "auto");
  }, [isOpen]);

  useEffect(() => {
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <div>
      {children}
      <div
        className={cn(
          "close-modal overflow-auto fixed right-0 left-0 bottom-0 top-0 w-full h-full transition-all duration-300",
          { "visible opacity-100": isOpen },
          { "invisible opacity-0": !isOpen }
        )}
        style={{ background: "rgba(0,0,0,.4)" }}
        onClick={onContainerClick}
      >
        <div
          className="close-modal my-auto relative w-full flex items-center justify-center px-4 py-5 mt-16 lg:mt-0"
          style={{ minHeight: "100vh" }}
        >
          <div className="p-3 md:p-10 w-full md:w-1/2 lg:w-1/3 shadow-lg bg-white rounded-lg">
            {body}
          </div>
        </div>
      </div>
    </div>
  );
}
