import React from "react";
import Link from "next/link";
import { useRecommendProfessorModal } from "./RecommendProfessorModal";
import { useFacultyModal } from "./FacultyModal";

export default function Faculty({ name, professors }) {
  const openRecommendProfessorModal = useRecommendProfessorModal(name);
  const openFacultyModal = useFacultyModal();

  return (
    <div className="bg-white shadow-lg py-10 px-5 rounded-md flex flex-col justify-between">
      <div>
        <span className="text-gray-700 font-light w-1/3 text-left block">
          دانشکده
        </span>
        <h3 className="w-full text-center text-gray-700 text-lg font-bold">
          {name}
        </h3>
      </div>
      <div className="flex flex-wrap my-8">
        {professors.slice(0, 4).map(professor => (
          <Link href={"/professors/" + professor.id} key={professor.id}>
            <a className="flex items-center w-1/2 py-1">
              <span className="w-2 h-2 rounded-full bg-gray-700 ml-2" />
              <span className="text-gray-700">{professor.name}</span>
            </a>
          </Link>
        ))}
      </div>
      <div className="block text-center">
        <a
          onClick={() => openFacultyModal(name, professors)}
          className="text-green cursor-pointer"
        >
          مشاهده همه اساتید
        </a>
        {/*<a*/}
        {/*      onClick={ openRecommendProfessorModal }*/}
        {/*      className="text-green cursor-pointer"*/}
        {/*  >پیشنهاد استاد جدید</a>*/}
      </div>
    </div>
  );
}
