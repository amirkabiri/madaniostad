import React from "react";
import Link from "next/link";
import { useRecommendProfessorModal } from "./RecommendProfessorModal";
import { useModal } from "../contexts/Modal";

export default function FacultyModal({ name, professors }) {
  const openRecommendProfessorModal = useRecommendProfessorModal(name);

  return (
    <>
      <span className="text-gray-700 font-light w-1/3 text-left block">
        دانشکده
      </span>
      <h3 className="w-full text-center text-gray-700 text-lg font-bold">
        {name}
      </h3>
      <div className="flex flex-wrap my-8">
        {professors.map(professor => (
          <Link href={"/professors/" + professor.id} key={professor.id}>
            <a className="flex items-center w-1/2 py-1">
              <span className="w-2 h-2 rounded-full bg-gray-700 ml-2" />
              <span className="text-gray-700">{professor.name}</span>
            </a>
          </Link>
        ))}
      </div>
      <div className="block text-center">
        <a
          onClick={openRecommendProfessorModal}
          className="text-green cursor-pointer"
        >
          پیشنهاد استاد جدید
        </a>
      </div>
    </>
  );
}

export function useFacultyModal() {
  const { open } = useModal();

  return (name, professors) =>
    open(<FacultyModal name={name} professors={professors} />);
}
