import React, { useState, useEffect, useRef } from "react";
import { getProfessors } from "../resources/professors";
import { dangerToast } from "../shared/toast";
import Router from "next/router";

let professorsFetched = false;
let cachedProfessors = [];

export default function NavSearch() {
  const inputRef = useRef();
  const [professors, setProfessors] = useState(cachedProfessors);

  const onFocus = () =>
    !professorsFetched &&
    getProfessors()
      .then(professors => {
        setProfessors(professors);
        professorsFetched = true;
        cachedProfessors = professors;
      })
      .catch(err => {
        console.log(err);
      });
  const onSubmit = e => {
    e.preventDefault();
    if (!inputRef.current) return;

    const [professor] = professors.filter(({ name }) =>
      new RegExp(`${inputRef.current.value}`).test(name)
    );
    if (!professor) return dangerToast("خطا!", "استادی با این نام یافت نشد");

    Router.push("/professors/" + professor.id);
  };

  return (
    <form
      onSubmit={onSubmit}
      className="rounded-full border border-gray-400 overflow-hidden flex my-3 lg:my-0"
    >
      <input
        onFocus={onFocus}
        ref={inputRef}
        type="text"
        className="py-2 px-4 text-gray-700"
        placeholder="دنبال کدوم استاد میگردی؟"
        list="professors-list"
      />
      <button
        className="flex items-center justify-center bg-green text-white ml-1 my-1 rounded-full w-10"
        type="submit"
      >
        <i className="fa fa-search" />
      </button>

      <datalist id="professors-list" dir="rtl" className="hidden">
        {professors.map(({ name, id }) => (
          <option value={name} key={id}>
            {name}
          </option>
        ))}
      </datalist>
    </form>
  );
}
