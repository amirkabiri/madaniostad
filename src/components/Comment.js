import React from "react";
import Button from "./Button";
import cn from "classnames";
import { useAuth } from "contexts/Auth";
import Link from "next/link";
import { useRouter } from "next/router";

export default function Comment({
  className,
  comment,
  setReplyTo,
  isChild,
  isLast,
}) {
  const { isLoggedIn } = useAuth();
  const router = useRouter();
  const { id } = router.query;

  return (
    <div
      className={cn("pb-2 mb-4 relative", className, {
        "border-b border-gray-300": !isLast,
      })}
    >
      {isChild && (
        <div
          style={{ width: 2, right: -12 }}
          className="h-full bg-blue-500 absolute"
        />
      )}
      <div className="flex items-end mb-2 justify-between">
        <span className="text-gray-700">ناشناس میگه:</span>
        {isLoggedIn && (
          <small
            key={id}
            className="text-blue-400 cursor-pointer"
            onClick={() => {
              setReplyTo(comment);

              const commentsSection = document.querySelector("#sendComment");
              if (commentsSection)
                commentsSection.scrollIntoView({ behavior: "smooth" });
            }}
          >
            پاسخ
          </small>
        )}
        {/*<div>*/}
        {/*    <Button*/}
        {/*        rounded="rounded"*/}
        {/*        background="bg-green"*/}
        {/*        className="ml-2">*/}
        {/*        <i className="fas fa-thumbs-up ml-1"/>*/}
        {/*        32*/}
        {/*    </Button>*/}
        {/*    <Button*/}
        {/*        rounded="rounded"*/}
        {/*        background="bg-red">*/}
        {/*        <i className="fas fa-thumbs-down ml-1"/>*/}
        {/*        32*/}
        {/*    </Button>*/}
        {/*</div>*/}
      </div>
      <p className="text-gray-600">{comment.text}</p>

      {!!comment.replies.length && (
        <div className="pr-10">
          {comment.replies.map((comment, index) => (
            <Comment
              isChild
              className="mt-4"
              isLast={true}
              setReplyTo={setReplyTo}
              comment={comment}
              key={index}
            />
          ))}
        </div>
      )}
    </div>
  );
}
