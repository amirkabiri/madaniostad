import React, { useState, useContext } from "react";
import { ModalContext } from "../contexts/Modal";
import FormGroup from "./FormGroup";
import Input from "./Input";
import Button from "./Button";
import { useLoginModal } from "./LoginModal";
import { ModalDescription, ModalTitle } from "./Modal";
import { createUser } from "../resources/users";
import { successToast } from "../shared/toast";

function SignUpModal() {
  const openLoginModal = useLoginModal();
  const { close: closeModal } = useContext(ModalContext);
  const [data, setData] = useState({
    email: "",
    name: "",
    studentId: "",
    password: "",
  });
  const [loading, setLoading] = useState(false);

  const onChange = ({ target }) =>
    setData(data => ({
      ...data,
      [target.getAttribute("data-key")]: target.value,
    }));
  const onSubmit = async e => {
    e.preventDefault();

    setLoading(true);

    try {
      await createUser(data);
      successToast("موفقیت", "ثبت نام انجام شد");
      closeModal();
    } catch (e) {
    } finally {
      setLoading(false);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <ModalTitle>ثبت نام در سایت</ModalTitle>
      <ModalDescription>
        برای بهره مندی از امکانات سایت، ثبت نام کنید
      </ModalDescription>

      <FormGroup title="نام و نام خانوادگی">
        <Input value={data.name} data-key="name" onChange={onChange} />
      </FormGroup>
      <FormGroup title="شماره دانشجویی">
        <Input
          value={data.studentId}
          data-key="studentId"
          onChange={onChange}
          placeholder="Example : 9718302222"
          dir="ltr"
        />
      </FormGroup>
      <FormGroup title="پست الکترونیکی">
        <Input
          value={data.email}
          data-key="email"
          onChange={onChange}
          placeholder="Example : your_email@gmail.com"
          dir="ltr"
        />
      </FormGroup>
      <FormGroup title="کلمه عبور">
        <Input
          value={data.password}
          data-key="password"
          onChange={onChange}
          type="password"
        />
      </FormGroup>
      <Button
        loading={loading}
        type="submit"
        background="bg-green"
        display="flex justify-center"
        className="w-full"
      >
        ثبت نام
      </Button>

      <div className="flex items-center justify-center">
        <span
          className="cursor-pointer text-green mt-4"
          onClick={openLoginModal}
        >
          ورود
        </span>
      </div>
    </form>
  );
}

export function useSignUpModal() {
  const { open } = useContext(ModalContext);

  return () => open(<SignUpModal />);
}
