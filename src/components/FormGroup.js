import React from "react";
import cn from "classnames";

export default function FormGroup({
  flex,
  margin,
  title,
  meta,
  className,
  children,
}) {
  return (
    <label className={cn(flex, margin, className)}>
      <div className="flex items-center justify-between">
        {title && <span className="text-sm text-gray-600 mb-1">{title}</span>}
        {meta}
      </div>
      {children}
    </label>
  );
}

FormGroup.defaultProps = {
  flex: "flex flex-col",
  margin: "mb-5",
};
