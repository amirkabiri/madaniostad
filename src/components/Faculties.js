import React from "react";
import Faculty from "./Faculty";
import { useRecommendFacultyModal } from "./RecommendFacultyModal";

export default function Faculties({ faculties }) {
  const openRecommendFacultyModal = useRecommendFacultyModal();

  return (
    <div className="w-full mt-20" id="faculties">
      <div className="flex items-center mb-10">
        <span className="w-20 h-20 flex items-center justify-center bg-gray-700 text-white">
          <i className="fa fa-archway text-xl" />
        </span>
        <div className="mr-5">
          <div className="flex justify-between mb-1">
            <h2 className="text-xl text-gray-700">دانشکده ها</h2>
            <a
              onClick={openRecommendFacultyModal}
              className="cursor-pointer text-green"
            >
              پیشنهاد دانشکده جدید
            </a>
          </div>
          <p className="text-gray-600 font-light">
            لیست دانشکده و اساتیدو میتونی اینجا ببینی
          </p>
        </div>
      </div>
      <div className="container mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5 px-5">
        {faculties.map(faculty => (
          <Faculty key={faculty.id} {...faculty} />
        ))}
      </div>
    </div>
  );
}

Faculties.defaultProps = {
  faculties: [],
};
