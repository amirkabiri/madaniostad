import React from "react";
import cn from "classnames";

export default function Input({
  padding,
  border,
  rounded,
  width,
  color,
  className,
  ...props
}) {
  return (
    <input
      className={cn(className, padding, border, rounded, width, color)}
      {...props}
    />
  );
}

Input.defaultProps = {
  padding: "px-4 py-2",
  border: "border border-gray-300",
  rounded: "rounded",
  width: "w-full",
  color: "text-gray",
};
