import React from "react";

export function ModalTitle({ children }) {
  return (
    <h2 className="w-full block text-center text-gray text-lg">{children}</h2>
  );
}

export function ModalDescription({ children }) {
  return <p className="mb-5 text-gray-600 text-center text-sm">{children}</p>;
}
