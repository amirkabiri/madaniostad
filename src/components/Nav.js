import React, { useContext } from "react";
import Link from "next/link";
import Button from "./Button";
import { useLoginModal } from "./LoginModal";
import { useSignUpModal } from "./SignUpModal";
import NavSearch from "./NavSearch";
import { useAuth } from "contexts/Auth";
import { successToast } from "shared/toast";

export default function Nav() {
  const { isLoggedIn, logout } = useAuth();
  const openLoginModal = useLoginModal();
  const openSingUpModal = useSignUpModal();

  const onLogout = () => {
    logout();
    successToast("موفقیت!", "از سایت خارج شدید");
  };

  return (
    <header className="fixed top-0 z-10 bg-white w-full shadow-lg py-2">
      <div className="mx-auto container flex flex-col lg:flex-row items-center justify-between">
        <Link href="/">
          <a className="flex items-center">
            <span className="font-black text-2xl ml-3">LOGO</span>
            <div className="flex flex-col">
              <h1 className="text-gray-700 font-bold text-lg">مدنی استاد</h1>
              <p className="text-gray-600 font-light">
                اینبار تو به اساتید نمره بده!
              </p>
            </div>
          </a>
        </Link>

        <NavSearch />

        <nav className="text-gray-700 flex items-center">
          <Link href="/#faculties">
            <a className="ml-4">دانشکده ها</a>
          </Link>
          <Link href="/#contact">
            <a className="ml-4">تماس با ما</a>
          </Link>
          {isLoggedIn ? (
            <Button onClick={onLogout} background="bg-red">
              <i className="fa fa-power-off ml-2" />
              خروج
            </Button>
          ) : (
            <>
              <a onClick={openSingUpModal} className="cursor-pointer ml-4">
                ثبت نام
              </a>
              <Button background="bg-green" onClick={openLoginModal}>
                <i className="fa fa-user ml-2" />
                ورود
              </Button>
            </>
          )}
        </nav>
      </div>
    </header>
  );
}
