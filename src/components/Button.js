import React from "react";
import Link from "next/link";
import cn from "classnames";
import Loading from "../icons/Loading";

export default function Button({
  position,
  loading,
  background,
  hover,
  rounded,
  padding,
  color,
  display,
  flex,
  href,
  children,
  className,
  ...props
}) {
  const elProps = {
    className: cn(
      className,
      rounded,
      padding,
      color,
      display,
      flex,
      hover,
      background,
      position
    ),
    ...props,
  };

  const loadingEl = loading && (
    <div
      className={cn(
        rounded,
        "flex items-center justify-center w-full h-full absolute left-0 right-0 top-0 bottom-0"
      )}
      style={{ background: "rgba(255,255,255,0.50)" }}
    >
      <Loading className="stroke-current text-gray" width={30} height={30} />
    </div>
  );

  if (!href) {
    return (
      <button {...elProps}>
        {children}
        {loadingEl}
      </button>
    );
  }
  return (
    <Link href={href}>
      <a {...elProps}>
        {children}
        {loading}
      </a>
    </Link>
  );
}

Button.defaultProps = {
  rounded: "rounded-full",
  padding: "px-4 py-2",
  color: "text-white",
  display: "inline-flex",
  flex: "items-center",
  hover: "hover:opacity-75 transition-color duration-300",
  background: "bg-gray",
  position: "relative",
  loading: false,
};
