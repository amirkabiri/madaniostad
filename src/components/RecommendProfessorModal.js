import React, { useContext } from "react";
import { ModalContext } from "../contexts/Modal";
import FormGroup from "./FormGroup";
import Input from "./Input";
import Button from "./Button";
import { ModalDescription, ModalTitle } from "./Modal";

function RecommendProfessorModal({ faculty }) {
  return (
    <>
      <ModalTitle>پیشنهاد استاد جدید</ModalTitle>
      <ModalDescription>
        لطفا استاد مورد نظر خود را از طریق فرم زیر معرفی کنین تا به دانشکده{" "}
        {faculty} اضافه شود
      </ModalDescription>

      <FormGroup title="نام استاد">
        <Input type="text" />
      </FormGroup>

      <Button
        background="bg-green"
        display="flex justify-center"
        className="w-full"
      >
        ارسال پیشنهاد
      </Button>
    </>
  );
}

export function useRecommendProfessorModal(faculty) {
  const { open } = useContext(ModalContext);

  return () => open(<RecommendProfessorModal faculty={faculty} />);
}
