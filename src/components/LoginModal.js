import React, { useState } from "react";
import { useModal } from "contexts/Modal";
import FormGroup from "./FormGroup";
import Input from "./Input";
import Button from "./Button";
import { useSignUpModal } from "./SignUpModal";
import { ModalDescription, ModalTitle } from "./Modal";
import { dangerToast, successToast } from "../shared/toast";
import { authenticate } from "resources/users";
import { useAuth } from "contexts/Auth";

function LoginModal() {
  const { login } = useAuth();
  const { close: closeModal } = useModal();
  const openSignUpModal = useSignUpModal();
  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [loading, setLoading] = useState(false);

  const onSubmit = async e => {
    e.preventDefault();

    setLoading(true);

    try {
      const { access_token } = await authenticate(data.email, data.password);
      login(access_token);
      closeModal();
      successToast("موفقیت!", "با موفقیت در سایت وارد شدید");
    } catch (e) {
    } finally {
      setLoading(false);
    }
  };
  const onChange = ({ target }) =>
    setData(data => ({
      ...data,
      [target.getAttribute("data-key")]: target.value,
    }));

  return (
    <form onSubmit={onSubmit}>
      <ModalTitle>ورود به سایت</ModalTitle>
      <ModalDescription>
        با ورود به سایت میتونی به اساتید نمره بدی و کامنت بذاری
      </ModalDescription>

      <FormGroup title="پست الکترونیکی">
        <Input
          value={data.email}
          data-key="email"
          onChange={onChange}
          type="text"
        />
      </FormGroup>
      <FormGroup
        title="کلمه عبور"
        meta={
          <a className="text-sm cursor-pointer text-green">فراموش کلمه عبور</a>
        }
      >
        <Input
          value={data.password}
          data-key="password"
          onChange={onChange}
          type="password"
        />
      </FormGroup>

      <Button
        loading={loading}
        type="submit"
        background="bg-green"
        display="flex justify-center"
        className="w-full"
      >
        ورود
      </Button>
      <div className="flex items-center justify-center">
        <span
          onClick={openSignUpModal}
          className="cursor-pointer text-green mt-4"
        >
          ثبت نام
        </span>
      </div>
    </form>
  );
}

export function useLoginModal() {
  const { open } = useModal();

  return () => open(<LoginModal />);
}
