import React, { useRef, useState } from "react";
import Comment from "components/Comment";
import Button from "components/Button";
import {
  getProfessorComments,
  sendProfessorComment,
} from "resources/professors";
import { useRouter } from "next/router";
import { useAuth } from "contexts/Auth";
import { useLoginModal } from "./LoginModal";
import { dangerToast, successToast } from "../shared/toast";

export default function Comments({ professor, setComments, comments }) {
  const commentRef = useRef();
  const router = useRouter();
  const { id } = router.query;
  const [loading, setLoading] = useState(false);
  const [replyTo, setReplyTo] = useState(null);
  const { isLoggedIn } = useAuth();
  const openLoginModal = useLoginModal();

  const sendComment = async () => {
    if (!commentRef.current) return;
    const text = commentRef.current.value.trim();

    if (!text.length) return dangerToast("خطا", "کامنت خود را وارد کنید");

    try {
      setLoading(true);
      await sendProfessorComment(id, text, replyTo?.id);
      commentRef.current.value = "";

      const comments = await getProfessorComments(professor.id);
      setComments(comments);
      successToast("موفقیت", "نظر شما ثبت شد");
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
      setReplyTo(null);
    }
  };

  return (
    <div className="container mx-auto px-5">
      <div className="bg-white rounded-md p-5 shadow-lg">
        <div className="flex items-center mb-5">
          <h2 className="font-bold text-gray-700 text-lg">
            نظرات کاربران | {comments.length}
          </h2>
          <span className="text-gray-700 mr-1">نظر</span>
        </div>

        <div>
          {comments
            .filter(comment => !comment.replyToId)
            .map((comment, index) => (
              <Comment
                setReplyTo={setReplyTo}
                comment={comment}
                key={index}
                isLast={
                  index ===
                  comments.filter(comment => !comment.replyToId).length - 1
                }
              />
            ))}
        </div>

        <div id="sendComment" className="relative">
          {replyTo && (
            <div className="text-sm text-gray-600 mb-1 mt-10 flex items-center justify-between">
              <div>
                پاسخ به :{" "}
                {replyTo.text.substr(0, 50) +
                  (replyTo.length > 50 ? " ..." : "")}
              </div>
              <i
                className="cursor-pointer p-5 fas fa-times text-red-500"
                onClick={() => setReplyTo(null)}
              ></i>
            </div>
          )}
          <textarea
            ref={commentRef}
            className="mt-0 w-full border border-gray-300 p-4 rounded-md text-gray-700"
            placeholder="نظر خود را بنویسید"
          />
          <Button
            loading={loading}
            onClick={sendComment}
            rounded="rounded"
            background="bg-green"
          >
            ارسال
          </Button>
          {!isLoggedIn && (
            <div
              className="text-gray-700 flex justify-center items-center w-full h-full absolute top-0 left-0 right-0 bottom-0"
              style={{ background: "rgba(255,255,255,.7)" }}
            >
              برای ارسال کامنت باید ابتدا در سایت
              <a
                onClick={openLoginModal}
                className="text-green cursor-pointer mr-1"
              >
                وارد شوید
              </a>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
