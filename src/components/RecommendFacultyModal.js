import React, { useContext } from "react";
import { ModalContext } from "../contexts/Modal";
import FormGroup from "./FormGroup";
import Input from "./Input";
import Button from "./Button";
import { ModalDescription, ModalTitle } from "./Modal";

function RecommendFacultyModal() {
  return (
    <>
      <ModalTitle>پیشنهاد دانشکده جدید</ModalTitle>
      <ModalDescription>
        لطفا دانشکده مورد نظر خود را از طریق فرم زیر به ما پیشنهاد بدین تا به
        لیست دانشکده ها اضافه کنیم
      </ModalDescription>

      <FormGroup title="نام دانشکده">
        <Input type="text" />
      </FormGroup>

      <Button
        background="bg-green"
        display="flex justify-center"
        className="w-full"
      >
        ارسال پیشنهاد
      </Button>
    </>
  );
}

export function useRecommendFacultyModal() {
  const { open } = useContext(ModalContext);

  return () => open(<RecommendFacultyModal />);
}
