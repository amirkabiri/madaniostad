import React, { createContext, useContext, useState } from "react";

export const defaultValue = {
  open: false,
  body: null,
};

export const ModalContext = createContext(defaultValue);

export function ModalProvider({ children }) {
  const [modal, setModal] = useState(defaultValue);

  const open = body =>
    setModal(modal => ({
      ...modal,
      open: true,
      body,
    }));
  const close = () =>
    setModal(modal => ({
      ...modal,
      open: false,
    }));

  return (
    <ModalContext.Provider
      value={{ open, close, body: modal.body, isOpen: modal.open }}
    >
      {children}
    </ModalContext.Provider>
  );
}

export function useModal() {
  return useContext(ModalContext);
}
