import React, { useContext, createContext, useState } from "react";
import { deleteToken, setToken } from "../shared/token";

export const defaultValue = {
  isLoggedIn: false,
  user: {},
};

export const AuthContext = createContext(defaultValue);

export function AuthProvider({ children }) {
  const [auth, setAuth] = useState(defaultValue);

  const setUser = user =>
    setAuth(auth => ({
      ...auth,
      user,
    }));
  const login = token => {
    setAuth(auth => ({ ...auth, isLoggedIn: true }));
    setToken(token);
  };
  const logout = () => {
    setAuth(auth => ({ ...auth, isLoggedIn: false }));
    deleteToken();
  };

  return (
    <AuthContext.Provider
      value={{
        ...auth,
        setUser,
        login,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}
