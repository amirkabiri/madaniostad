import Head from "next/head";
import Nav from "../components/Nav.js";
import Header from "../components/Header.js";
import Contact from "../components/Contact";
import Faculties from "../components/Faculties";
import { apiHandleError } from "../shared/api";
import { getFaculties } from "../resources/faculties";

export default function Home({ faculties }) {
  return (
    <>
      <Head>
        <title>مدنی استاد | ارزیابی اساتید</title>
      </Head>

      <Nav />

      <Header />

      <Faculties faculties={faculties} />

      <Contact />
    </>
  );
}

export async function getServerSideProps() {
  return {
    props: {
      faculties: await apiHandleError(getFaculties, []),
    },
  };
}
