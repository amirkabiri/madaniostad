import React from "react";
import Providers from "providers";

import "izitoast/dist/css/iziToast.min.css";
import "../../public/fontawesome-free/css/all.min.css";
import "../../styles/tailwind.build.css";
import "../../styles/main.css";

export default function App({ Component, pageProps }) {
  return (
    <Providers>
      <Component {...pageProps} />
    </Providers>
  );
}
