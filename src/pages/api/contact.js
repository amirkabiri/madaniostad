import axios from "axios";
import NextCors from "nextjs-cors";
import {
  TELEGRAM_BOT_TOKEN,
  TELEGRAM_GROUP_CHAT_ID,
} from "../../shared/config";

function errorRes(res, error) {
  res.statusCode = 422;
  res.json({ error });
}

export default async (req, res) => {
  await NextCors(req, res, {
    // Options
    methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
    origin: "*",
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  });

  const { name, phone, message } = req.body;

  if (typeof name !== "string" || !name.length) {
    return errorRes(res, "لطفا نام خود را وارد کنید");
  } else if (typeof phone !== "string" || !phone.length) {
    return errorRes(res, "لطفا شماره موبایل خود را وارد کنید");
  } else if (typeof message !== "string" || !message.length) {
    return errorRes(res, "لطفا پیام خود را وارد کنید");
  }

  try {
    await axios.post(
      `https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendmessage`,
      {
        chat_id: TELEGRAM_GROUP_CHAT_ID,
        text: `New Message \n ${name}(${phone}) \n ${message}`,
      }
    );
    res.statusCode = 200;
    res.json({ ok: true });
  } catch (e) {
    res.statusCode = 500;
    res.json({ error: "خطای غیر منتظره ای رخ داده است" });
  }
};
