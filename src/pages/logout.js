import React, { useEffect } from "react";
import Router from "next/router";

export default function Logout() {
  useEffect(() => {
    Router.push("/");
  }, []);

  return <div>Logout</div>;
}
