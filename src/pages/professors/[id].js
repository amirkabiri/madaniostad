import { useRef, useState } from "react";
import Nav from "components/Nav";
import { getProfessorByID, getProfessorComments } from "resources/professors";
import { apiHandleError } from "shared/api";
import { getFacultyByID } from "resources/faculties";
import Head from "next/head";
import FormGroup from "components/FormGroup";
import Input from "components/Input";
import Comments from "components/Comments";

export default function Professors({ professor, faculty, ...props }) {
  console.log(props.comments);
  const [comments, setComments] = useState(props.comments);

  return (
    <>
      <Head>
        <title>پروفایل {professor.name} | مدنی استاد</title>
      </Head>

      <Nav />

      <div className="container mx-auto px-5 mb-10 mt-48 lg:mt-20">
        <div
          className="bg-white shadow-lg rounded-md flex items-center p-5"
          style={{ width: "max-content" }}
        >
          <img
            className="w-32 h-32 rounded-full"
            src="https://api.adorable.io/avatars/285/test.png"
            alt="professor avatar"
          />
          <div className="text-gray-700 mr-4">
            <div className="flex flex-wrap items-center">
              <h2 className="text-lg font-bold">{professor.name}</h2>
              <span className="bg-green-400 text-white rounded-full py-1 px-3 mr-4 text-sm">
                دانشیار
              </span>
            </div>
            <div className="flex items-center mt-2 mb-1">
              <span>دانشکده :</span>
              <span className="font-bold mr-2">{faculty}</span>
            </div>
            <div className="flex items-center">
              <span>امتیاز از 10 :</span>
              <span className="font-bold mr-2">8.9</span>
            </div>
          </div>
        </div>
      </div>

      {/*<div className="container mx-auto px-5 my-10">*/}
      {/*  <div className="bg-white rounded-md p-5 shadow-lg">*/}
      {/*    <div className="flex items-center">*/}
      {/*      <h2 className="font-bold text-gray-700 text-lg">*/}
      {/*        چه درسایی با این استاد برداشتی؟ | {comments.length}*/}
      {/*      </h2>*/}
      {/*      <span className="text-gray-700 mr-1">نظر</span>*/}
      {/*    </div>*/}
      {/*    <div className="grid grid-cols-1 md:grid-cols-4 gap-x-3">*/}
      {/*      <FormGroup title="عنوان درس">*/}
      {/*        <Input />*/}
      {/*      </FormGroup>*/}
      {/*      <FormGroup title="نمره">*/}
      {/*        <Input />*/}
      {/*      </FormGroup>*/}
      {/*      <FormGroup title="سال تحصیلی">*/}
      {/*        <Input />*/}
      {/*      </FormGroup>*/}
      {/*      <FormGroup title="ترم">*/}
      {/*        <Input />*/}
      {/*      </FormGroup>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</div>*/}

      <Comments
        professor={professor}
        setComments={setComments}
        comments={comments}
      />
    </>
  );
}
Professors.defaultProps = {
  professor: {
    name: "",
  },
  faculty: "",
};

export async function getServerSideProps({ params }) {
  const professor = await apiHandleError(
    () => getProfessorByID(params.id),
    null
  );
  if (!professor) return { props: {} };

  const faculty = await apiHandleError(
    () => getFacultyByID(professor.facultyId),
    { name: "" }
  );

  return {
    props: {
      professor,
      faculty: faculty.name,
      comments: await apiHandleError(() => getProfessorComments(params.id), []),
    },
  };
}
