import api, { apiSuccessOutput } from "../shared/api";

export async function getProfessorByID(professorID) {
  return apiSuccessOutput(await api.get("professors/" + professorID));
}

export async function getProfessorComments(professorID) {
  return apiSuccessOutput(await api.get(`professors/${professorID}/comments`));
}

export async function sendProfessorComment(
  professorID,
  text,
  replyToId = null
) {
  const body = { text };
  if (replyToId) body.replyToId = replyToId;

  return apiSuccessOutput(
    await api.post(`professors/${professorID}/comments`, body)
  );
}

export async function getProfessors() {
  return apiSuccessOutput(await api.get("professors"));
}
