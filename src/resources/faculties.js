import api, { apiHandleError, apiSuccessOutput } from "../shared/api";

export async function getFaculties() {
  return apiSuccessOutput(await api.get("Faculties"));
}

export async function getFacultyProfessors(facultyID) {
  return apiSuccessOutput(await api.get(`Faculties/${facultyID}/Professors`));
}

export async function getFacultyByID(facultyID) {
  return apiSuccessOutput(await api.get("Faculties/" + facultyID));
}
