import axios from "axios";
import api, { apiSuccessOutput } from "../shared/api";

export async function authenticateClientSide(email, password) {
  const res = await axios.post("/api/authenticate", { email, password });
  return res.data;
}

export async function authenticate(email, password) {
  const res = await api.post("users/authenticate", {
    grant_Type: "Password",
    username: email,
    password,
  });
  return res.data;
}

export async function createUser({ email, password, studentId, name }) {
  return apiSuccessOutput(
    await api.post("users", { email, password, studentId, name })
  );
}
