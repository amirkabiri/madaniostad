import axios from "axios";

export function sendContactMessage(data) {
  return axios.post("/api/contact", data);
}
